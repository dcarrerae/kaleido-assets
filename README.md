# README #

## Clone
git clone git@bitbucket.org:dcarrerae/kaleido-assets.git

## Requirements
Install Pug - Example macOsx

* npm install pug-cli -g
* pug -V                    # versión pug

Install Sass

* gem install sass
* sass -v

Install Bourbon (mixin Sass)

* gem install bourbon
* sudo gem install -n /usr/local/bin bourbon   # Alternative installation
* bourbon -v

##How to use
* cd kaleido-assets
* bower install
* pug assets-tester.pug -w -P
* sass --watch sass/assets.sass:css/assets.css
* open assets-tester.html in browser