$( document ).ready(function() {

	$( ".test-short" ).click(function()
	{
		let title1 = 'texto 1 As a Matter of Fact';
		let title2 = 'texto 2 As far as I know';
		let title3 = 'texto 3 As far as I remember / recall';
		let title4 = 'texto 4 Original Poster, Original Post';
		let title5 = 'texto 5 Oh my God';
		let title6 = 'texto 6 Thank You Very Much';
		let title7 = 'You made my Day';

		let textoOriginal = $(this)[0].parentElement.parentElement.children[2].children[0].innerText;
		let elementText = $(this)[0].parentElement.parentElement.children[2].children[0];
		let elementItem = $(this)[0].parentElement.parentElement.children[2];

		if($(elementText).hasClass('text-medium'))
		{
			$(elementText).removeClass('text-medium');
			$(elementItem).removeClass('item-test');
		}
		if($(elementText).hasClass('text-large'))
		{
			$(elementText).removeClass('text-large');
			$(elementItem).removeClass('item-test');
		}

		$(elementText).toggleClass( "text-short" ); // Agregamos text-short (cambiar de texto)
		$(elementItem).toggleClass( "item-test" ); 	// Agregamos item-test (para animación contenedor)
		let ss = $(this)[0].parentElement.parentElement.children[2].children[0].id; // Identificador eg: a01

		/**
		 * Comprobamos si a01 contoene la class text-short
		 */
		if($('#' + ss).hasClass('text-short'))
		{
			if($('p#' + ss).hasClass('text-short'))
			{
				setTimeout(function()
				{
					console.log("entramos setTimeout 1");
					if($('p#' + ss).hasClass('text-short'))
					{
						console.log($('p#' + ss).hasClass('text-short'));
						$('.text-short#' + ss).text(title1);
						console.log("animación 1 id: " + ss);
						if($('p#' + ss).hasClass('text-short'))
						{
							setTimeout(function()
							{
								console.log("entramos setTimeout 2");
								if($('p#' + ss).hasClass('text-short'))
								{
									console.log($('p#' + ss).hasClass('text-short'));
									$('.text-short#' + ss).text(title2);
									console.log("animación 2 id: " + ss);
									if($('p#' + ss).hasClass('text-short'))
									{
										setTimeout(function()
										{
											console.log("entramos setTimeout 3");
											if($('p#' + ss).hasClass('text-short'))
											{
												console.log($('p#' + ss).hasClass('text-short'));
												$('.text-short#' + ss).text(title3);
												console.log("animación 3 id: " + ss);
												if($('p#' + ss).hasClass('text-short'))
												{
													setTimeout(function()
													{
														console.log("entramos setTimeout 4");
														if($('p#' + ss).hasClass('text-short'))
														{
															console.log($('p#' + ss).hasClass('text-short'));
															$('.text-short#' + ss).text(title4);
															console.log("animación 4 id: " + ss);

															if($('p#' + ss).hasClass('text-short'))
															{
																setTimeout(function()
																{
																	console.log("entramos setTimeout 5");
																	if($('p#' + ss).hasClass('text-short'))
																	{
																		console.log($('p#' + ss).hasClass('text-short'));
																		$('.text-short#' + ss).text(title5);
																		console.log("animación 5 id: " + ss);
																		if($('p#' + ss).hasClass('text-short'))
																		{
																			setTimeout(function()
																			{
																				console.log("entramos setTimeout 6");
																				if($('p#' + ss).hasClass('text-short'))
																				{
																					console.log($('p#' + ss).hasClass('text-short'));
																					$('.text-short#' + ss).text(title6);
																					console.log("animación 6 id: " + ss);

																					if($('p#' + ss).hasClass('text-short'))
																					{
																						setTimeout(function()
																						{
																							console.log("entramos setTimeout 7");
																							if($('p#' + ss).hasClass('text-short'))
																							{
																								console.log($('p#' + ss).hasClass('text-short'));
																								console.log("texto original: " + title7);
																								$('.text-short#' + ss).text(title7);
																								$('p#' + ss).toggleClass( "text-short" );
																								$(elementItem).toggleClass( "item-test" );
																								console.log("animación 8");
																							}else $('#' + ss).text(title7);
																						},2000);
																					}else $('#' + ss).text(title7);
																				}else $('#' + ss).text(title7);
																			},2000);
																		}else $('#' + ss).text(title7);
																	}else $('#' + ss).text(title7);
																},2000);
															}else $('#' + ss).text(title7);
														}else $('#' + ss).text(title7);
													},2000);
												}else $('#' + ss).text(title7);
											}else $('#' + ss).text(title7);
										},2000);
									}else $('#' + ss).text(title7);
								}else $('#' + ss).text(title7);
							},2000);
						}else $('#' + ss).text(title7);
					} else $('#' + ss).text(title7);
				},2000);
			}else $('#' + ss).text(title7);
		}else $('#' + ss).text(title7);
	});

	$( ".test-medium" ).click(function()
	{
		let medium1 = 'An identifier tempts a tiger.';
		let medium2 = 'The performing trolley uses the lesson.';
		let medium3 = 'A rumored football graphs. The cured offender.';
		let medium4 = 'A viewer elaborates. On top of a honey intervenes.';
		let medium5 = 'Repondez sil vous plait (French: Please reply)';
		let medium6 = '(Short Message Service (=SMS)) and WhatsApp Abbreviations';
		let medium7 = 'What is the difference between Abbreviations and Acronyms?';

		let textoOriginal = $(this)[0].parentElement.parentElement.children[2].children[0].innerText;
		let elementText = $(this)[0].parentElement.parentElement.children[2].children[0];
		let elementItem = $(this)[0].parentElement.parentElement.children[2];

		if($(elementText).hasClass('text-short'))
		{
			$(elementText).removeClass('text-short');
			$(elementItem).removeClass('item-test');
		}
		if($(elementText).hasClass('text-large'))
		{
			$(elementText).removeClass('text-large');
			$(elementItem).removeClass('item-test');
		}

		$(elementText).toggleClass( "text-medium" ); // Agregamos text-short (cambiar de texto)
		$(elementItem).toggleClass( "item-test" ); 	// Agregamos item-test (para animación contenedor)
		let ss = $(this)[0].parentElement.parentElement.children[2].children[0].id; // Identificador eg: a01

		/**
		 * Comprobamos si a01 contoene la class text-short
		 */
		if($('#' + ss).hasClass('text-medium'))
		{
			if($('p#' + ss).hasClass('text-medium'))
			{
				setTimeout(function()
				{
					if($('p#' + ss).hasClass('text-medium'))
					{
						$('.text-medium#' + ss).text(medium1);
						if($('p#' + ss).hasClass('text-medium'))
						{
							setTimeout(function()
							{
								if($('p#' + ss).hasClass('text-medium'))
								{
									$('.text-medium#' + ss).text(medium2);
									if($('p#' + ss).hasClass('text-medium'))
									{
										setTimeout(function()
										{
											if($('p#' + ss).hasClass('text-medium'))
											{
												$('.text-medium#' + ss).text(medium3);
												if($('p#' + ss).hasClass('text-medium'))
												{
													setTimeout(function()
													{
														if($('p#' + ss).hasClass('text-medium'))
														{
															$('.text-medium#' + ss).text(medium4);
															if($('p#' + ss).hasClass('text-medium'))
															{
																setTimeout(function()
																{
																	if($('p#' + ss).hasClass('text-medium'))
																	{
																		$('.text-medium#' + ss).text(medium5);
																		if($('p#' + ss).hasClass('text-medium'))
																		{
																			setTimeout(function()
																			{
																				if($('p#' + ss).hasClass('text-medium'))
																				{
																					$('.text-medium#' + ss).text(medium6);
																					if($('p#' + ss).hasClass('text-medium'))
																					{
																						setTimeout(function()
																						{
																							if($('p#' + ss).hasClass('text-medium'))
																							{
																								$('.text-medium#' + ss).text(medium7);
																								$('p#' + ss).toggleClass( "text-medium" );
																								$(elementItem).toggleClass( "item-test" );
																							}else $('#' + ss).text(medium7);
																						},2000);
																					}else $('#' + ss).text(medium7);
																				}else $('#' + ss).text(medium7);
																			},2000);
																		}else $('#' + ss).text(medium7);
																	}else $('#' + ss).text(medium7);
																},2000);
															}else $('#' + ss).text(medium7);
														}else $('#' + ss).text(medium7);
													},2000);
												}else $('#' + ss).text(medium7);
											}else $('#' + ss).text(medium7);
										},2000);
									}else $('#' + ss).text(medium7);
								}else $('#' + ss).text(medium7);
							},2000);
						}else $('#' + ss).text(medium7);
					} else $('#' + ss).text(medium7);
				},2000);
			}else $('#' + ss).text(medium7);
		}else $('#' + ss).text(medium7);
	});

	$( ".test-large" ).click(function()
	{
		var large1 = 'The morning gggg g ggggg gggg jjjjj j jjj j jgjghthththh h hthhtj jgjgj gjgjjg jgjgjgjjgjg A disorder swims below the message. The duck ties the developing keystroke. Will a design refuse the graffito?'; //'The morning director changes. A disorder swims below the message. The duck ties the developing keystroke. Will a design refuse the graffito?';
		var large2 = 'His disease raves underneath the appalling pedantry. A handy heterosexual nests above the flat cassette. An uncertain translator orbits.';
		var large3 = 'The tangent crossroad degenerates. A frown paces next to the warming autumn. Past the faulty profit dashes an enlightened listener. A romance figures the scum.';
		var large4 = 'A platform breathes a gene. The allowed cook gloves a swallowed soundtrack. The postponed luck errs outside the nominal chord. The catalog.';
		let large5 = 'Distance does not make the miles, but the people ... As you remember if you start to forget the forgetting of forgetting me and I begin to remember.';
		let large6 = 'There are silences that speak and words that say nothing ...If you cry over losing the sun, the tears will prevent you from seeing the stars';
		let large7 = 'The heart is the only broken instrument that works even. The smile is the only virus that does not harm the soul.'
		let textoOriginal = $(this)[0].parentElement.parentElement.children[2].children[0].innerText;
		let elementText = $(this)[0].parentElement.parentElement.children[2].children[0];
		let elementItem = $(this)[0].parentElement.parentElement.children[2];

		if($(elementText).hasClass('text-short'))
		{
			$(elementText).removeClass('text-short');
			$(elementItem).removeClass('item-test');
		}

		if($(elementText).hasClass('text-medium'))
		{
			$(elementText).removeClass('text-medium');
			$(elementItem).removeClass('item-test');
		}

		$(elementText).toggleClass( "text-large" ); // Agregamos text-short (cambiar de texto)
		$(elementItem).toggleClass( "item-test" ); 	// Agregamos item-test (para animación contenedor)
		let ss = $(this)[0].parentElement.parentElement.children[2].children[0].id; // Identificador eg: a01

		/**
		 * Comprobamos si a01 contoene la class text-short
		 */
		if($('#' + ss).hasClass('text-large'))
		{
			if($('p#' + ss).hasClass('text-large'))
			{
				setTimeout(function()
				{
					if($('p#' + ss).hasClass('text-large'))
					{
						$('.text-large#' + ss).text(large1);
						if($('p#' + ss).hasClass('text-large'))
						{
							setTimeout(function()
							{
								if($('p#' + ss).hasClass('text-large'))
								{
									$('.text-large#' + ss).text(large2);
									if($('p#' + ss).hasClass('text-large'))
									{
										setTimeout(function()
										{
											if($('p#' + ss).hasClass('text-large'))
											{
												$('.text-large#' + ss).text(large3);
												if($('p#' + ss).hasClass('text-large'))
												{
													setTimeout(function()
													{
														if($('p#' + ss).hasClass('text-large'))
														{
															$('.text-large#' + ss).text(large4);
															if($('p#' + ss).hasClass('text-large'))
															{
																setTimeout(function()
																{
																	if($('p#' + ss).hasClass('text-large'))
																	{
																		$('.text-large#' + ss).text(large5);
																		if($('p#' + ss).hasClass('text-large'))
																		{
																			setTimeout(function()
																			{
																				if($('p#' + ss).hasClass('text-large'))
																				{
																					$('.text-large#' + ss).text(large6);
																					if($('p#' + ss).hasClass('text-large'))
																					{
																						setTimeout(function()
																						{
																							if($('p#' + ss).hasClass('text-large'))
																							{
																								$('.text-large#' + ss).text(large7);
																								$('p#' + ss).toggleClass( "text-large" );
																								$(elementItem).toggleClass( "item-test" );
																							}else $('#' + ss).text(large7);
																						},2000);
																					}else $('#' + ss).text(large7);
																				}else $('#' + ss).text(large7);
																			},2000);
																		}else $('#' + ss).text(large7);
																	}else $('#' + ss).text(large7);
																},2000);
															}else $('#' + ss).text(large7);
														}else $('#' + ss).text(large7);
													},2000);
												}else $('#' + ss).text(large7);
											}else $('#' + ss).text(large7);
										},2000);
									}else $('#' + ss).text(large7);
								}else $('#' + ss).text(large7);
							},2000);
						}else $('#' + ss).text(large7);
					} else $('#' + ss).text(large7);
				},2000);
			}else $('#' + ss).text(large7);
		}else $('#' + ss).text(large7);
	});
});
